/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.messages.response;

import com.aeroman.dataentry.messages.response.base.Header;
import com.aeroman.dataentry.messages.response.interfaces.IResponseUser;
import com.aeroman.dataentry.models.entities.User;

/**
 *
 * @author rex2002xp
 */
public class ResponseUser implements IResponseUser{

    public Header header;
    public Iterable<User> data;

    public ResponseUser() {
        this.header = new Header();
    }
    
    
    @Override
    public void setData(Iterable<User> data) {
        this.data = data;
    }

    @Override
    public void setCode(Integer code) {
        this.header.code = code;
    }

    @Override
    public void setDescription(String description) {
        this.header.description = description;
    }
    
}
