/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.messages.response.base;

import java.io.Serializable;

/**
 *
 * @author rex2002xp
 */
public class Header implements Serializable{
    public Integer code;
    public String description;

    public Header() {
        this.code = 200;
        this.description = "Operacion Exitosa";
    }
    
}
