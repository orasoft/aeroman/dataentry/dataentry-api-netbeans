/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.messages.response;

import com.aeroman.dataentry.messages.response.base.Header;
import java.io.Serializable;

/**
 *
 * @author rex2002xp
 */
public class ResponseVersion implements Serializable{
    public Header header;
    public String version;
    
    public ResponseVersion() {
        this.header = new Header();
        this.version = "1.0.4";
    }
    
    public ResponseVersion(String version) {
        this.header = new Header();
        this.version = version;
    }
}
