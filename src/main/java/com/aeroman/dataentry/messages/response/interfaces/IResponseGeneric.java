/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.messages.response.interfaces;

/**
 *
 * @author rex2002xp
 */
public interface IResponseGeneric<T> {
    void setData(Iterable<T> data);
    void setCode(Integer code);
    void setDescription(String description);
}
