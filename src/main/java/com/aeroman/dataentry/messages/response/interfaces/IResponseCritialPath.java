/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.messages.response.interfaces;

import com.aeroman.dataentry.models.entities.CriticalPaths;

/**
 *
 * @author rex2002xp
 */
public interface IResponseCritialPath extends IResponseGeneric<CriticalPaths>{
    
}
