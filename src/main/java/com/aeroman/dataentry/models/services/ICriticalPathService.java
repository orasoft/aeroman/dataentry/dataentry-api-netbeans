/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.services;

import com.aeroman.dataentry.models.entities.CriticalPaths;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author rex2002xp
 */
public interface ICriticalPathService {
    public List<CriticalPaths> findAll();

    public CriticalPaths findById(UUID id);

    public CriticalPaths save(CriticalPaths criticalPaths);

    public void delete(UUID id);
}
