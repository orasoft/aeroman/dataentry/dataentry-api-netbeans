/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.services;

import com.aeroman.dataentry.models.entities.DailyFocus;
import java.util.List;
import java.util.UUID;
/**
 *
 * @author rex2002xp
 */
public interface IDailyFocusService {
    public List<DailyFocus> findAll();

    public DailyFocus findById(UUID id);

    public DailyFocus save(DailyFocus dailyFocus);

    public void delete(UUID id);
}
