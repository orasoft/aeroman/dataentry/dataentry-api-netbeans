/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.services;

import com.aeroman.dataentry.models.entities.User;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author rex2002xp
 */
public interface IUserService {
    public List<User> findAll();

    public User findById(UUID id);
    
    public Integer findByUsername(String username, String password);

    public User save(User user);

    public void delete(UUID id);
}
