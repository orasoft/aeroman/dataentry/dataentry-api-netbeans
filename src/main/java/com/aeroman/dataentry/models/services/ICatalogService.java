/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.services;

import com.aeroman.dataentry.models.resultset.AircraftInfoResultSet;
import com.aeroman.dataentry.models.resultset.CompanyResultSet;
import com.aeroman.dataentry.models.resultset.CustomerResultSet;
import com.aeroman.dataentry.models.resultset.ItemResultSet;
import java.util.List;

/**
 *
 * @author rex2002xp
 */
public interface ICatalogService {
    List<CompanyResultSet> getCompanies();

    List<CustomerResultSet> getCustomers(String company);

    List<AircraftInfoResultSet> getAircraft(String company, String customer);

    List<ItemResultSet> getItems(String itemId);
}
