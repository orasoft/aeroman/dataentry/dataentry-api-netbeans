/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.resultset;

/**
 *
 * @author rex2002xp
 */
public class AircraftInfoResultSet {
    private String aircraft;
    private Integer woNumber;
    private Integer woCorr;
    private Integer woItem;

    public String getAircraft() {
        return aircraft;
    }

    public void setAircraft(String aircraft) {
        this.aircraft = aircraft;
    }

    public Integer getWoNumber() {
        return woNumber;
    }

    public void setWoNumber(Integer woNumber) {
        this.woNumber = woNumber;
    }

    public Integer getWoCorr() {
        return woCorr;
    }

    public void setWoCorr(Integer woCorr) {
        this.woCorr = woCorr;
    }

    public Integer getWoItem() {
        return woItem;
    }

    public void setWoItem(Integer woItem) {
        this.woItem = woItem;
    }
}
