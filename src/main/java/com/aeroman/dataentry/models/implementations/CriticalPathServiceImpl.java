/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.implementations;

import com.aeroman.dataentry.models.services.ICriticalPathService;
import com.aeroman.dataentry.models.entities.CriticalPaths;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.aeroman.dataentry.models.dao.ICriticalPathDao;
import org.springframework.stereotype.Service;

/**
 *
 * @author rex2002xp
 */
@Service
public class CriticalPathServiceImpl implements ICriticalPathService{
    @Autowired
    private ICriticalPathDao criticalPathDao;

    @Override
    @Transactional(readOnly = true)
    public List<CriticalPaths> findAll() {
        return (List<CriticalPaths>) criticalPathDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public CriticalPaths findById(UUID id) {
        return criticalPathDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public CriticalPaths save(CriticalPaths criticalPaths) {
        return criticalPathDao.save(criticalPaths);
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        criticalPathDao.deleteById(id);
    }
}
