/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.implementations;

import com.aeroman.dataentry.models.dao.IUserDao;
import com.aeroman.dataentry.models.entities.User;
import com.aeroman.dataentry.models.services.IUserService;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author rex2002xp
 */
@Service 
public class UserServiceImpl implements IUserService{
    
    @Autowired
    private IUserDao userDao;

    @Override
    @Transactional(readOnly = true)
    public List<User> findAll() {
        return (List<User>) this.userDao.findAll();
    }

    @Override
    public User findById(UUID id) {
        return userDao.findById(id).orElse(null);
    }
    
   
    @Override
    public User save(User user) {
        return userDao.save(user);
    }

    @Override
    public void delete(UUID id) {
        userDao.deleteById(id);
    }

    @Override
    public Integer findByUsername(String username, String password) {
        List<Object[]> records = userDao.getUserByUsername(username, password);
        Integer response = 0;
        for (Object[] obj : records) {
                response = Integer.parseInt(obj[0].toString()) ;
            }
        return response;
    }
    
}
