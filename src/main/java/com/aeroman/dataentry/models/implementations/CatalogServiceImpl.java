/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.implementations;

import com.aeroman.dataentry.models.dao.ICatalogDao;
import com.aeroman.dataentry.models.resultset.AircraftInfoResultSet;
import com.aeroman.dataentry.models.resultset.CompanyResultSet;
import com.aeroman.dataentry.models.resultset.CustomerResultSet;
import com.aeroman.dataentry.models.resultset.ItemResultSet;
import com.aeroman.dataentry.models.services.ICatalogService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author rex2002xp
 */
@Service
public class CatalogServiceImpl implements ICatalogService {
    @Autowired
    private ICatalogDao catalogDao;

    @Override
    @Transactional(readOnly = true)
    public List<CompanyResultSet> getCompanies() {
        List<Object[]> records = this.catalogDao.getCompanies();

        List<CompanyResultSet> response = new ArrayList<>();

        try{

            for (Object[] obj : records) {
                CompanyResultSet item = new CompanyResultSet();
                item.setLabel(obj[0].toString());
                item.setValue(obj[0].toString());
                response.add(item);
            }
        } catch (Exception ex) {}

        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerResultSet> getCustomers(String company) {

        List<Object[]> records = this.catalogDao.getCustomers(company);

        List<CustomerResultSet> response = new ArrayList<>();

        try {
            for (Object[] obj : records) {
                CustomerResultSet item = new CustomerResultSet();
                item.setLabel(obj[0].toString());
                item.setValue(obj[0].toString());
                response.add(item);
            }
        } catch (Exception e) {

        }

        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public List<AircraftInfoResultSet> getAircraft(String company, String customer) {
        List<Object[]> records = this.catalogDao.getAircraft(company, customer);

        List<AircraftInfoResultSet> response = new ArrayList<>();

        try {
            for (Object[] obj : records) {
                AircraftInfoResultSet item = new AircraftInfoResultSet();
                item.setAircraft(obj[0].toString());
                item.setWoNumber(Integer.parseInt(obj[1].toString()));
                item.setWoCorr(Integer.parseInt(obj[2].toString()));
                item.setWoItem(Integer.parseInt(obj[3].toString()));
                response.add(item);
            }
        } catch (Exception ex) {}

        return response;
    }

    @Override
    public List<ItemResultSet> getItems(String itemId) {
        List<Object[]> records = this.catalogDao.getItems(itemId);

        List<ItemResultSet> response = new ArrayList<>();

        try {
            for (Object[] obj : records) {
                ItemResultSet item = new ItemResultSet();
                item.setLabel(obj[0].toString());
                item.setValue(obj[1].toString());
                response.add(item);
            }
        } catch (Exception ex) {}

        return response;
    }
}
