/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.aeroman.dataentry.models.services.IDailyFocusService;
import com.aeroman.dataentry.models.dao.IDailyFocusDao;
import com.aeroman.dataentry.models.entities.DailyFocus;
import java.util.List;
import java.util.UUID;
/**
 *
 * @author rex2002xp
 */
@Service
public class DailyFocusServiceImpl implements IDailyFocusService {
    @Autowired
    private IDailyFocusDao dailyFocusDao;

    @Override
    @Transactional(readOnly = true)
    public List<DailyFocus> findAll() {
        return (List<DailyFocus>) dailyFocusDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public DailyFocus findById(UUID id) {
        return dailyFocusDao.findById(id).orElse(null);
    }

    @Override
    public DailyFocus save(DailyFocus dailyFocus) {
        return dailyFocusDao.save(dailyFocus);
    }

    @Override
    public void delete(UUID id) {
        dailyFocusDao.deleteById(id);
    }
    
}
