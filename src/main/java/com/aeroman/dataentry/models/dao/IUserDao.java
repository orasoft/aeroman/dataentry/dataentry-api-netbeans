/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.dao;

import com.aeroman.dataentry.models.entities.User;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author rex2002xp
 */
public interface IUserDao extends CrudRepository<User, UUID> {
    @Query(value = "SELECT COUNT(1) AS EXIST FROM ODS.USERS WHERE USERNAME = ?1 AND PASSWORD = ?2", nativeQuery = true)
    List<Object[]> getUserByUsername(String username, String password);
}
