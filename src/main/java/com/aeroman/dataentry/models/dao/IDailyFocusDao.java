/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.dao;

import org.springframework.data.repository.CrudRepository;
import com.aeroman.dataentry.models.entities.DailyFocus;
import java.util.UUID;
/**
 *
 * @author rex2002xp
 */
public interface IDailyFocusDao extends CrudRepository<DailyFocus, UUID> {
    
}
