/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.dao;

import com.aeroman.dataentry.models.entities.CriticalPaths;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author rex2002xp
 */
public interface ICriticalPathDao extends CrudRepository<CriticalPaths, UUID> {

}
