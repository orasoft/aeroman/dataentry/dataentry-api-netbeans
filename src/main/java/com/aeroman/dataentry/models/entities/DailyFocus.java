/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @author rex2002xp
 */
@Entity
@Table(name = "C_MROH_DAILY_FOCUS", schema = "ODS")
public class DailyFocus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

//    @Column(name = "COMPANY")
//    private String company;
//
//    @Column(name = "CUSTOMER")
//    private String customer;
    @Column(name = "COMPLETE", columnDefinition = "NUMBER(10,4)")
    private double complete;

    @Column(name = "REASON", length = 50)
    private String reason;

    @Column(name = "WORK_ORDER", length = 25)
    private String workOrder;

    @Column(name = "AIRCRAFT", length = 25)
    private String aircraft;

    @Column(name = "TASK", length = 250)
    private String task;

    @Column(name = "DATE_ID")
    @Temporal(TemporalType.DATE)
    private Date dateId;

    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date createdAt;

    @Column(name = "CREATED_USER", length = 50)
    private String createdBy;

    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date updatedAt;

    @Column(name = "UPDATED_USER", length = 50)
    private String updatedBy;

    @Column(name = "MROH_SOURCE", length = 20)
    private String mrohSource;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAircraft() {
        return aircraft;
    }

    public void setAircraft(String aircraft) {
        this.aircraft = aircraft;
    }

    public String getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(String workOrder) {
        this.workOrder = workOrder;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public double getComplete() {
        return complete;
    }

    public void setComplete(double complete) {
        this.complete = complete;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getDateId() {
        return dateId;
    }

    public void setDateId(Date dateId) {
        this.dateId = dateId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getMrohSource() {
        return mrohSource;
    }

    public void setMrohSource(String mrohSource) {
        this.mrohSource = mrohSource;
    }

    @PrePersist
    public void prePersist() {
        createdAt = new Date();
        updatedAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        updatedAt = new Date();
    }

//    public String getCompany() {
//        return company;
//    }
//
//    public void setCompany(String company) {
//        this.company = company;
//    }
//
//    public String getCustomer() {
//        return customer;
//    }
//
//    public void setCustomer(String customer) {
//        this.customer = customer;
//    }
}
