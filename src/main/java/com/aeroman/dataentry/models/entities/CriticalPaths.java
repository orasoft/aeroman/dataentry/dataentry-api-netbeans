/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @author rex2002xp
 */
@Entity
@Table(name = "C_MROH_CRITICAL_PATHS", schema = "ODS")
public class CriticalPaths implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @Column(name = "CRITICAL_PATH", length = 250)
    private String criticalPath;
    @Column(name = "TAT_IMPACT")
    private Long tatImpact;
    @Column(name = "STATUS", length = 50)
    private char status;
    @Column(name = "MITIGATION_PLAN", length = 250)
    private String mitigationPlan;
    @Column(name = "RESPOSIBLE", length = 50)
    private String responsible;
    @Column(name = "WORK_ORDER", length = 25)
    private String workOrder;
    @Column(name = "AIRCRAFT", length = 25)
    private String aircraft;
    @Column(name = "STATUS_FINAL", length = 50)
    private char statusFinal;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date createdAt;
    @Column(name = "CREATED_USER", length = 50)
    private String createdBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date updatedAt;
    @Column(name = "UPDATED_USER", length = 50)
    private String updatedBy;
    @Column(name = "MROH_SOURCE", length = 20)
    private String mrohSource;
//    @Column(name = "COMPANY")
//    private String company;
//    @Column(name = "CUSTOMER")
//    private String customer;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAircraft() {
        return aircraft;
    }

    public void setAircraft(String aircraft) {
        this.aircraft = aircraft;
    }

    public String getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(String workOrder) {
        this.workOrder = workOrder;
    }

    public String getCriticalPath() {
        return criticalPath;
    }

    public void setCriticalPath(String criticalPath) {
        this.criticalPath = criticalPath;
    }

    public Long getTatImpact() {
        return tatImpact;
    }

    public void setTatImpact(Long tatImpact) {
        this.tatImpact = tatImpact;
    }

    public String getMitigationPlan() {
        return mitigationPlan;
    }

    public void setMitigationPlan(String mitigationPlan) {
        this.mitigationPlan = mitigationPlan;
    }

    public String getResponsible() {
        return responsible;
    }

    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public char getStatusFinal() {
        return statusFinal;
    }

    public void setStatusFinal(char statusfinal) {
        this.statusFinal = statusfinal;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getMrohSource() {
        return mrohSource;
    }

    public void setMrohSource(String mrohSource) {
        this.mrohSource = mrohSource;
    }

//    public String getCompany() {
//        return company;
//    }
//    public void setCompany(String company) {
//        this.company = company;
//    }
//
//    public String getCustomer() {
//        return customer;
//    }
//
//    public void setCustomer(String customer) {
//        this.customer = customer;
//    }
    @PrePersist
    public void prePersist() {
        createdAt = new Date();
        updatedAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        updatedAt = new Date();
    }
}
