/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.models.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @author rex2002xp
 */
@Entity
@Table(name = "CATALOG_ITEM", schema = "ODS")
public class CatalogItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @Column(name = "LABEL")
    private String label;
    @Column(name = "VALUE")
    private String value;
    @Column(name = "STATUS")
    private char status;
    @Column(name = "CREATED_AT")
    @Temporal(TemporalType.DATE)
    private Date createdAt;
    @Column(name = "CREATED_BY")
    private String createdBy;
    @Column(name = "UPDATED_AT")
    @Temporal(TemporalType.DATE)
    private Date updatedAt;
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    @ManyToOne
    @JoinColumn
    private Catalog catalog;
    
}
