/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.aeroman.dataentry.models.services.IDailyFocusService;
import com.aeroman.dataentry.models.entities.DailyFocus;
import com.aeroman.dataentry.messages.response.ResponseDailyFocus;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
/**
 *
 * @author rex2002xp
 */
@RestController
@RequestMapping("/api/v1/dataentry/daily_focus")
public class DailyFocusRestController {
    @Autowired
    private IDailyFocusService dailyFocusService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping
    public ResponseDailyFocus index() {
        ResponseDailyFocus response = new ResponseDailyFocus();
        List<DailyFocus> data = dailyFocusService.findAll();
        response.setData(data);
        return response;
    }
    
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseDailyFocus create(@RequestBody DailyFocus dailyFocus) {
        dailyFocus.setCreatedBy("system");
        dailyFocus.setUpdatedBy("system");

        DailyFocus recordCreated = dailyFocusService.save(dailyFocus);

        ResponseDailyFocus response = new ResponseDailyFocus();
        List<DailyFocus> data = new ArrayList<>();
        data.add(recordCreated);
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/{id}")
    public ResponseDailyFocus show(@PathVariable UUID id) {
        DailyFocus record = dailyFocusService.findById(id);

        ResponseDailyFocus response = new ResponseDailyFocus();
        List<DailyFocus> data = new ArrayList<>();
        data.add(record);
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PutMapping("/{id}")
    public ResponseDailyFocus update(@RequestBody DailyFocus dailyFocus, @PathVariable UUID id) {
        DailyFocus dailyFocusActual = dailyFocusService.findById(id);

        // dailyFocusActual.setAircraft(dailyFocus.getAircraft());
        // dailyFocusActual.setWorkOrder(dailyFocus.getWorkOrder());
        dailyFocusActual.setTask(dailyFocus.getTask());
        dailyFocusActual.setComplete(dailyFocus.getComplete());
        dailyFocusActual.setReason(dailyFocus.getReason());
        dailyFocusActual.setDateId(dailyFocus.getDateId());

        DailyFocus recordUpdated = dailyFocusService.save(dailyFocusActual);

        ResponseDailyFocus response = new ResponseDailyFocus();
        List<DailyFocus> data = new ArrayList<>();
        data.add(recordUpdated);
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseDailyFocus delete(@PathVariable UUID id) {
        dailyFocusService.delete(id);

        ResponseDailyFocus response = new ResponseDailyFocus();
        List<DailyFocus> data = new ArrayList<>();
        response.setData(data);
        return response;
    }
}
