/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.controllers;

import com.aeroman.dataentry.messages.response.ResponseCriticalPath;
import com.aeroman.dataentry.models.entities.CriticalPaths;
import com.aeroman.dataentry.models.services.ICriticalPathService;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author rex2002xp
 */
@RestController
@RequestMapping("/api/v1/dataentry/critical_paths")
public class CriticalPathRestController {

    @Autowired
    private ICriticalPathService criticalPathService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping
    public ResponseCriticalPath index() {
        ResponseCriticalPath response = new ResponseCriticalPath();
        List<CriticalPaths> data = criticalPathService.findAll();
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/{id}")
    public ResponseCriticalPath show(@PathVariable UUID id) {
        CriticalPaths record = criticalPathService.findById(id);
        ResponseCriticalPath response = new ResponseCriticalPath();
        List<CriticalPaths> data = new ArrayList<>();
        data.add(record);
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseCriticalPath create(@RequestBody CriticalPaths criticalPaths) {
        criticalPaths.setCreatedBy("system");
        criticalPaths.setUpdatedBy("system");
        CriticalPaths recordCreated = criticalPathService.save(criticalPaths);

        ResponseCriticalPath response = new ResponseCriticalPath();
        List<CriticalPaths> data = new ArrayList<>();
        data.add(recordCreated);
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PutMapping("/{id}")
    public ResponseCriticalPath update(@RequestBody CriticalPaths criticalPaths, @PathVariable UUID id) {
        CriticalPaths criticalPathsActual = criticalPathService.findById(id);

        //criticalPathsActual.setAircraft(criticalPaths.getAircraft());
        //criticalPathsActual.setWorkOrder(criticalPaths.getWorkOrder());
        criticalPathsActual.setCriticalPath(criticalPaths.getCriticalPath());
        criticalPathsActual.setTatImpact(criticalPaths.getTatImpact());
        criticalPathsActual.setMitigationPlan(criticalPaths.getMitigationPlan());
        criticalPathsActual.setResponsible(criticalPaths.getResponsible());
        criticalPathsActual.setStatus(criticalPaths.getStatus());
        criticalPathsActual.setStatusFinal(criticalPaths.getStatusFinal());

        CriticalPaths recordUpdated = criticalPathService.save(criticalPathsActual);

        ResponseCriticalPath response = new ResponseCriticalPath();
        List<CriticalPaths> data = new ArrayList<>();
        data.add(recordUpdated);
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseCriticalPath delete(@PathVariable UUID id) {
        criticalPathService.delete(id);
        ResponseCriticalPath response = new ResponseCriticalPath();
        List<CriticalPaths> data = new ArrayList<>();
        response.setData(data);
        return response;
    }
}
