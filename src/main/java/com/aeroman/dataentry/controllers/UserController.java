/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.controllers;

import com.aeroman.dataentry.messages.response.ResponseUser;
import com.aeroman.dataentry.models.entities.User;
import com.aeroman.dataentry.models.services.IUserService;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rex2002xp
 */
@RestController
@RequestMapping("/api/v1/admin/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping
    public ResponseUser index() {
        ResponseUser response = new ResponseUser();
        List<User> data = userService.findAll();
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseUser create(@RequestBody User user) {
        
        String stringHash = sha1(user.getPassword());
        user.setPassword(stringHash);
        User recordCreated = userService.save(user);

        ResponseUser response = new ResponseUser();
        List<User> data = new ArrayList<>();
        data.add(recordCreated);
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/{id}")
    public ResponseUser show(@PathVariable UUID id) {
        User record = userService.findById(id);

        ResponseUser response = new ResponseUser();
        List<User> data = new ArrayList<>();
        data.add(record);
        response.setData(data);
        return response;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseUser delete(@PathVariable UUID id) {
        userService.delete(id);

        ResponseUser response = new ResponseUser();
        List<User> data = new ArrayList<>();
        response.setData(data);
        return response;
    }

    public String sha1(String input) {
        String sha1 = null;
        try {
            MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
            msdDigest.update(input.getBytes("UTF-8"), 0, input.length());
            sha1 = DatatypeConverter.printHexBinary(msdDigest.digest());
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            Logger.getLogger(MessageDigest.class.getName()).log(Level.SEVERE, null, e);
        }
        return sha1;
    }
}
