/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.controllers;

import com.aeroman.dataentry.messages.response.ResponseVersion;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rex2002xp
 */
@RestController
@RequestMapping("/api/v1/version")
public class VersionController {
    
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping
    public ResponseVersion version() {
        return new ResponseVersion("1.0.8");
    }
}
