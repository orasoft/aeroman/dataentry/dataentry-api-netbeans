/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroman.dataentry.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.aeroman.dataentry.models.resultset.AircraftInfoResultSet;
import com.aeroman.dataentry.models.resultset.CompanyResultSet;
import com.aeroman.dataentry.models.resultset.CustomerResultSet;
import com.aeroman.dataentry.models.resultset.ItemResultSet;
import com.aeroman.dataentry.models.services.ICatalogService;


/**
 *
 * @author rex2002xp
 */
@RestController
@RequestMapping("/api/v1/dataentry/catalog")
public class CatalogRestController {

    @Autowired
    private ICatalogService catalogService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/items/{id}")
    public List<ItemResultSet> getItems(@PathVariable String id) {
        // ResponseCriticalPath response = new ResponseCriticalPath();
        List<ItemResultSet> data = catalogService.getItems(id);
        return data;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/company")
    public List<CompanyResultSet> getCompanies() {
        // ResponseCriticalPath response = new ResponseCriticalPath();
        List<CompanyResultSet> data = catalogService.getCompanies();
        return data;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/customer/{company}")
    public List<CustomerResultSet> getCustomers(@PathVariable String company) {
        // ResponseCriticalPath response = new ResponseCriticalPath();
        List<CustomerResultSet> data = catalogService.getCustomers(company);
        return data;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/aircraft/{company}/{customer}")
    public List<AircraftInfoResultSet> getAircraft(@PathVariable String company, @PathVariable String customer) {
        List<AircraftInfoResultSet> data = catalogService.getAircraft(company, customer);
        return data;
    }

}
